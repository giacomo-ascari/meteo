﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MeteoTrentino
{
    [Serializable()]

    public class DatiStazione
    {
        Mutex mtxAccessoAlFileXML = new Mutex();
        string data;
        double tmin;
        double tmax;
        double rain;
        List<Temperatura> temperature;
        List<Precipitazioni> precipitazioni;
        List<Venti> venti;
        List<Radiazioni> radiazioni;
        List<UmiditaRelativa> umiditaRelativa;

        public static DatiStazione GetDatiStazione(string fileName)
        {
            try
            {
                DatiStazione datiStazione = new DatiStazione();
                StreamReader sr = new StreamReader(fileName);
                List<string> list = new List<string>();
                while (!sr.EndOfStream) list.Add(sr.ReadLine());
                sr.Close();
                StreamWriter sw = new StreamWriter(fileName + ".elab");
                sw.AutoFlush = true;
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Contains("<datiOggi")) list[i] = "<DatiStazione>";
                    else
                    {

                        list[i] = list[i].Replace("datiOggi", "DatiStazione");

                        list[i] = list[i].Replace("temperature", "Temperature");
                        list[i] = list[i].Replace("<temperatura_aria UM=\"??C\">", "<Temperatura><um>°C</um>");
                        list[i] = list[i].Replace("</temperatura_aria>", "</Temperatura>");
                        list[i] = list[i].Replace("temperatura", "t");

                        list[i] = list[i].Replace("<precipitazione UM=\"mm\">", "<Precipitazioni><um>mm</um>");
                        list[i] = list[i].Replace("pioggia", "p");
                        list[i] = list[i].Replace("precipitazione", "Precipitazioni");
                        list[i] = list[i].Replace("precipitazioni", "Precipitazioni");

                        list[i] = list[i].Replace("<vento_al_suolo UM_VV=\"m/s\" UM_DV=\"gN\">", "<Venti><um>m/s</um>");
                        list[i] = list[i].Replace("vento_al_suolo", "Venti");
                        list[i] = list[i].Replace("venti", "Venti");

                        list[i] = list[i].Replace("<radiazioneglobale UM=\"W/mq\">", "<Radiazioni><um>W/mq</um>");
                        list[i] = list[i].Replace("radiazioneglobale", "Radiazioni");
                        list[i] = list[i].Replace("radiazione", "Radiazioni");

                        list[i] = list[i].Replace("<umidita_relativa UM=\"%\">", "<umidita_relativa><um>%</um>");
                        list[i] = list[i].Replace("umidita_relativa", "UmiditaRelativa");

                    }
                    sw.WriteLine(list[i]);
                }
                sw.Close();
                XmlSerializer ser = new XmlSerializer(typeof(DatiStazione));
                using (XmlReader reader = XmlReader.Create(fileName + ".elab"))
                {
                    datiStazione = (DatiStazione)ser.Deserialize(reader);
                }
                return datiStazione;
            }
            catch
            {
                return null;
            }
            
        }

        [System.Xml.Serialization.XmlElement("data")]
        public string Data { get => data; set => data = value; }
        [System.Xml.Serialization.XmlElement("tmin")]
        public double Tmin { get => tmin; set => tmin = value; }
        [System.Xml.Serialization.XmlElement("tmax")]
        public double Tmax { get => tmax; set => tmax = value; }
        [System.Xml.Serialization.XmlElement("rain")]
        public double Rain { get => rain; set => rain = value; }
        public Mutex MtxAccessoAlFileXML { get => mtxAccessoAlFileXML; set => mtxAccessoAlFileXML = value; }
        public List<Temperatura> Temperature { get => temperature; set => temperature = value; }
        public List<Precipitazioni> Precipitazioni { get => precipitazioni; set => precipitazioni = value; }
        public List<Venti> Venti { get => venti; set => venti = value; }
        public List<Radiazioni> Radiazioni { get => radiazioni; set => radiazioni = value; }
        public List<UmiditaRelativa> UmiditaRelativa { get => umiditaRelativa; set => umiditaRelativa = value; }
    }

    [Serializable()]

    public class Temperatura
    {
        DateTime data;
        double t;
        string um = "°C";
        public Temperatura() { }
        public Temperatura(DateTime data, double t, string um)
        {
            this.data = data;
            this.t = t;
            this.um = um;
        }
        [System.Xml.Serialization.XmlElement("data")]
        public DateTime Data { get => data; set => data = value; }
        [System.Xml.Serialization.XmlElement("t")]
        public double T { get => t; set => t = value; }
        [System.Xml.Serialization.XmlElement("um")]
        public string Um { get => um; set => um = value; }
    }
    public class Precipitazioni
    {
        DateTime data;
        double p;
        string um = "mm";
        public Precipitazioni() { }

        public Precipitazioni(DateTime data, double p, string um)
        {
            this.data = data;
            this.p = p;
            this.um = um;
        }
        [System.Xml.Serialization.XmlElement("data")]
        public DateTime Data { get => data; set => data = value; }
        [System.Xml.Serialization.XmlElement("p")]
        public double P { get => p; set => p = value; }
        [System.Xml.Serialization.XmlElement("um")]
        public string Um { get => um; set => um = value; }
    }
    public class Venti
    {
        DateTime data;
        double v;
        double d;
        string um = "m/s - gN";
        public Venti() { }

        public Venti(DateTime data, double v, double d, string um)
        {
            this.data = data;
            this.v = v;
            this.d = d;
            this.um = um;
        }
        [System.Xml.Serialization.XmlElement("data")]
        public DateTime Data { get => data; set => data = value; }
        [System.Xml.Serialization.XmlElement("v")]
        public double V { get => v; set => v = value; }
        [System.Xml.Serialization.XmlElement("d")]
        public double D { get => d; set => d = value; }
        [System.Xml.Serialization.XmlElement("um")]
        public string Um { get => um; set => um = value; }
    }
    public class Radiazioni
    {
        DateTime data;
        double rsg;
        string um = "W/mq";
        public Radiazioni() { }

        public Radiazioni(DateTime data, double rsg, string um)
        {
            this.data = data;
            this.rsg = rsg;
            this.um = um;
        }
        [System.Xml.Serialization.XmlElement("data")]
        public DateTime Data { get => data; set => data = value; }
        [System.Xml.Serialization.XmlElement("rsg")]
        public double Rsg { get => rsg; set => rsg = value; }
        [System.Xml.Serialization.XmlElement("um")]
        public string Um { get => um; set => um = value; }
    }
    public class UmiditaRelativa
    {
        DateTime data;
        double rh;
        string um = "%";
        public UmiditaRelativa() { }

        public UmiditaRelativa(DateTime data, double rh, string um)
        {
            this.data = data;
            this.rh = rh;
            this.um = um;
        }
        [System.Xml.Serialization.XmlElement("data")]
        public DateTime Data { get => data; set => data = value; }
        [System.Xml.Serialization.XmlElement("rh")]
        public double Rh { get => rh; set => rh = value; }
        [System.Xml.Serialization.XmlElement("um")]
        public string Um { get => um; set => um = value; }
    }
}
