﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MeteoTrentino
{
    [Serializable()]
    public class ArrayOfAnagrafica : List<Anagrafica>
    {
        public ArrayOfAnagrafica() { }
        public static ArrayOfAnagrafica GetArrayOfAnagrafica(string fileName)
        {
            ArrayOfAnagrafica arrayOfAnagrafica = new ArrayOfAnagrafica();
            StreamReader sr = new StreamReader(fileName);
            List<string> list = new List<string>();
            while (!sr.EndOfStream) list.Add(sr.ReadLine());
            sr.Close();
            StreamWriter sw = new StreamWriter(fileName + ".elab");
            sw.AutoFlush = true;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Contains("<ArrayOfAnagrafica")) list[i] = "<ArrayOfAnagrafica>";
                else list[i] = list[i].Replace("anagrafica", "Anagrafica");
                sw.WriteLine(list[i]);
            }
            sw.Close();
            XmlSerializer ser = new XmlSerializer(typeof(ArrayOfAnagrafica));
            using (XmlReader reader = XmlReader.Create(fileName + ".elab"))
            {
                arrayOfAnagrafica = (ArrayOfAnagrafica)ser.Deserialize(reader);
            }
            return arrayOfAnagrafica;
        }
    }

    [Serializable()]
    public class Anagrafica
    {
        string codice;
        string nome;
        string nomeBreve;
        int quota;
        double latitudine;
        double longitudine;
        double east;
        double north;
        DateTime inizio;
        DateTime fine;
        double score;
        DatiStazione dati;

        public Anagrafica() { }

        public Anagrafica(string codice, string nome, string nomeBreve, int quota, double latitudine, double longitudine, double east, double north, DateTime inizio, DateTime fine)
        {
            this.codice = codice;
            this.nome = nome;
            this.nomeBreve = nomeBreve;
            this.quota = quota;
            this.latitudine = latitudine;
            this.longitudine = longitudine;
            this.east = east;
            this.north = north;
            this.inizio = inizio;
            this.fine = fine;
        }

        [System.Xml.Serialization.XmlElement("codice")]
        public string Codice { get => codice; set => codice = value; }
        [System.Xml.Serialization.XmlElement("nome")]

        public string Nome { get => nome; set => nome = value; }
        [System.Xml.Serialization.XmlElement("nomebreve")]

        public string NomeBreve { get => nomeBreve; set => nomeBreve = value; }
        [System.Xml.Serialization.XmlElement("quota")]

        public int Quota { get => quota; set => quota = value; }
        [System.Xml.Serialization.XmlElement("latitudine")]

        public double Latitudine { get => latitudine; set => latitudine = value; }
        [System.Xml.Serialization.XmlElement("longitudine")]

        public double Longitudine { get => longitudine; set => longitudine = value; }
        [System.Xml.Serialization.XmlElement("east")]

        public double East { get => east; set => east = value; }
        [System.Xml.Serialization.XmlElement("north")]
        public double North { get => north; set => north = value; }
        [System.Xml.Serialization.XmlElement("inizio")]
        public DateTime Inizio { get => inizio; set => inizio = value; }
        [System.Xml.Serialization.XmlElement("fine")]
        public DateTime Fine { get => fine; set => fine = value; }
        public double Score { get => score; set => score = value; }
        public DatiStazione Dati { get => dati; set => dati = value; }
    }
}
