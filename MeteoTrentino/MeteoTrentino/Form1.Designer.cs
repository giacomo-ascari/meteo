﻿namespace MeteoTrentino
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.anagraficheLBox = new System.Windows.Forms.ListBox();
            this.cercaTBox = new System.Windows.Forms.TextBox();
            this.cercaBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.umidPlot = new OxyPlot.WindowsForms.PlotView();
            this.label4 = new System.Windows.Forms.Label();
            this.radPlot = new OxyPlot.WindowsForms.PlotView();
            this.label3 = new System.Windows.Forms.Label();
            this.ventiPlot = new OxyPlot.WindowsForms.PlotView();
            this.label2 = new System.Windows.Forms.Label();
            this.precPlot = new OxyPlot.WindowsForms.PlotView();
            this.temperatureLbl = new System.Windows.Forms.Label();
            this.tempPlot = new OxyPlot.WindowsForms.PlotView();
            this.infoLbl = new System.Windows.Forms.Label();
            this.latLongStazioneLbl = new System.Windows.Forms.Label();
            this.varieStazioneLbl = new System.Windows.Forms.Label();
            this.codiceStazioneLbl = new System.Windows.Forms.Label();
            this.nomeStazioneLbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.anagraficheLBox);
            this.splitContainer1.Panel1.Controls.Add(this.cercaTBox);
            this.splitContainer1.Panel1.Controls.Add(this.cercaBtn);
            this.splitContainer1.Panel1MinSize = 150;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Controls.Add(this.infoLbl);
            this.splitContainer1.Panel2.Controls.Add(this.latLongStazioneLbl);
            this.splitContainer1.Panel2.Controls.Add(this.varieStazioneLbl);
            this.splitContainer1.Panel2.Controls.Add(this.codiceStazioneLbl);
            this.splitContainer1.Panel2.Controls.Add(this.nomeStazioneLbl);
            this.splitContainer1.Panel2MinSize = 550;
            this.splitContainer1.Size = new System.Drawing.Size(1116, 489);
            this.splitContainer1.SplitterDistance = 257;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "MeteoTrentino";
            // 
            // anagraficheLBox
            // 
            this.anagraficheLBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.anagraficheLBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.anagraficheLBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.anagraficheLBox.FormattingEnabled = true;
            this.anagraficheLBox.ItemHeight = 16;
            this.anagraficheLBox.Location = new System.Drawing.Point(4, 72);
            this.anagraficheLBox.Name = "anagraficheLBox";
            this.anagraficheLBox.Size = new System.Drawing.Size(248, 404);
            this.anagraficheLBox.TabIndex = 2;
            this.anagraficheLBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.anagraficheLBox_DrawItem);
            this.anagraficheLBox.SelectedIndexChanged += new System.EventHandler(this.anagraficheLBox_SelectedIndexChanged);
            // 
            // cercaTBox
            // 
            this.cercaTBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cercaTBox.Location = new System.Drawing.Point(3, 46);
            this.cercaTBox.Name = "cercaTBox";
            this.cercaTBox.Size = new System.Drawing.Size(196, 20);
            this.cercaTBox.TabIndex = 1;
            this.cercaTBox.TextChanged += new System.EventHandler(this.cercaTBox_TextChanged);
            this.cercaTBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cercaTBox_KeyPress);
            // 
            // cercaBtn
            // 
            this.cercaBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cercaBtn.Location = new System.Drawing.Point(205, 44);
            this.cercaBtn.Name = "cercaBtn";
            this.cercaBtn.Size = new System.Drawing.Size(46, 23);
            this.cercaBtn.TabIndex = 0;
            this.cercaBtn.Text = "Cerca";
            this.cercaBtn.UseVisualStyleBackColor = true;
            this.cercaBtn.Click += new System.EventHandler(this.cercaBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.umidPlot);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.radPlot);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.ventiPlot);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.precPlot);
            this.panel1.Controls.Add(this.temperatureLbl);
            this.panel1.Controls.Add(this.tempPlot);
            this.panel1.Location = new System.Drawing.Point(3, 166);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(844, 318);
            this.panel1.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 804);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 20);
            this.label5.TabIndex = 13;
            this.label5.Text = "Umidità relativa";
            // 
            // umidPlot
            // 
            this.umidPlot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.umidPlot.Location = new System.Drawing.Point(1, 827);
            this.umidPlot.Name = "umidPlot";
            this.umidPlot.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.umidPlot.Size = new System.Drawing.Size(823, 175);
            this.umidPlot.TabIndex = 14;
            this.umidPlot.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.umidPlot.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.umidPlot.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 603);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Radiazione";
            // 
            // radPlot
            // 
            this.radPlot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radPlot.Location = new System.Drawing.Point(1, 626);
            this.radPlot.Name = "radPlot";
            this.radPlot.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.radPlot.Size = new System.Drawing.Size(823, 175);
            this.radPlot.TabIndex = 12;
            this.radPlot.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.radPlot.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.radPlot.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 402);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Venti";
            // 
            // ventiPlot
            // 
            this.ventiPlot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ventiPlot.Location = new System.Drawing.Point(1, 425);
            this.ventiPlot.Name = "ventiPlot";
            this.ventiPlot.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.ventiPlot.Size = new System.Drawing.Size(823, 175);
            this.ventiPlot.TabIndex = 10;
            this.ventiPlot.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.ventiPlot.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.ventiPlot.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Precipitazioni";
            // 
            // precPlot
            // 
            this.precPlot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.precPlot.Location = new System.Drawing.Point(1, 224);
            this.precPlot.Name = "precPlot";
            this.precPlot.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.precPlot.Size = new System.Drawing.Size(823, 175);
            this.precPlot.TabIndex = 8;
            this.precPlot.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.precPlot.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.precPlot.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // temperatureLbl
            // 
            this.temperatureLbl.AutoSize = true;
            this.temperatureLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.temperatureLbl.Location = new System.Drawing.Point(3, 0);
            this.temperatureLbl.Name = "temperatureLbl";
            this.temperatureLbl.Size = new System.Drawing.Size(100, 20);
            this.temperatureLbl.TabIndex = 4;
            this.temperatureLbl.Text = "Temperature";
            // 
            // tempPlot
            // 
            this.tempPlot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tempPlot.Location = new System.Drawing.Point(1, 23);
            this.tempPlot.Name = "tempPlot";
            this.tempPlot.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.tempPlot.Size = new System.Drawing.Size(823, 175);
            this.tempPlot.TabIndex = 6;
            this.tempPlot.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.tempPlot.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.tempPlot.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // infoLbl
            // 
            this.infoLbl.AutoSize = true;
            this.infoLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infoLbl.Location = new System.Drawing.Point(6, 132);
            this.infoLbl.Name = "infoLbl";
            this.infoLbl.Size = new System.Drawing.Size(17, 16);
            this.infoLbl.TabIndex = 5;
            this.infoLbl.Text = "...";
            // 
            // latLongStazioneLbl
            // 
            this.latLongStazioneLbl.AutoSize = true;
            this.latLongStazioneLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.latLongStazioneLbl.Location = new System.Drawing.Point(6, 54);
            this.latLongStazioneLbl.Name = "latLongStazioneLbl";
            this.latLongStazioneLbl.Size = new System.Drawing.Size(20, 18);
            this.latLongStazioneLbl.TabIndex = 3;
            this.latLongStazioneLbl.Text = "...";
            // 
            // varieStazioneLbl
            // 
            this.varieStazioneLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.varieStazioneLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.varieStazioneLbl.Location = new System.Drawing.Point(531, 12);
            this.varieStazioneLbl.Name = "varieStazioneLbl";
            this.varieStazioneLbl.Size = new System.Drawing.Size(307, 151);
            this.varieStazioneLbl.TabIndex = 2;
            this.varieStazioneLbl.Text = "...";
            this.varieStazioneLbl.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // codiceStazioneLbl
            // 
            this.codiceStazioneLbl.AutoSize = true;
            this.codiceStazioneLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codiceStazioneLbl.Location = new System.Drawing.Point(5, 29);
            this.codiceStazioneLbl.Name = "codiceStazioneLbl";
            this.codiceStazioneLbl.Size = new System.Drawing.Size(21, 20);
            this.codiceStazioneLbl.TabIndex = 1;
            this.codiceStazioneLbl.Text = "...";
            // 
            // nomeStazioneLbl
            // 
            this.nomeStazioneLbl.AutoSize = true;
            this.nomeStazioneLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomeStazioneLbl.Location = new System.Drawing.Point(4, 4);
            this.nomeStazioneLbl.Name = "nomeStazioneLbl";
            this.nomeStazioneLbl.Size = new System.Drawing.Size(30, 25);
            this.nomeStazioneLbl.TabIndex = 0;
            this.nomeStazioneLbl.Text = "...";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1116, 489);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(750, 400);
            this.Name = "Form1";
            this.Text = "Meteo Trentino";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox cercaTBox;
        private System.Windows.Forms.Button cercaBtn;
        private System.Windows.Forms.ListBox anagraficheLBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label nomeStazioneLbl;
        private System.Windows.Forms.Label codiceStazioneLbl;
        private System.Windows.Forms.Label varieStazioneLbl;
        private System.Windows.Forms.Label latLongStazioneLbl;
        private System.Windows.Forms.Label infoLbl;
        private System.Windows.Forms.Label temperatureLbl;
        private OxyPlot.WindowsForms.PlotView tempPlot;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private OxyPlot.WindowsForms.PlotView precPlot;
        private System.Windows.Forms.Label label5;
        private OxyPlot.WindowsForms.PlotView umidPlot;
        private System.Windows.Forms.Label label4;
        private OxyPlot.WindowsForms.PlotView radPlot;
        private System.Windows.Forms.Label label3;
        private OxyPlot.WindowsForms.PlotView ventiPlot;
    }
}

