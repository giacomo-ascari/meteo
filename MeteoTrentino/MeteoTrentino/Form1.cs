﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;

namespace MeteoTrentino
{
    public partial class Form1 : Form
    {
        ArrayOfAnagrafica listaStazioni;
        WebClient client = new WebClient();

        public Form1()
        {
            InitializeComponent();
            listaStazioni = ArrayOfAnagrafica.GetArrayOfAnagrafica("listaStazioni.xml");
            foreach (Anagrafica anagrafica in listaStazioni) anagraficheLBox.Items.Add(anagrafica.Nome);
            nomeStazioneLbl.Parent.Controls.SetChildIndex(nomeStazioneLbl, 1);
        }

        private void cercaBtn_Click(object sender, EventArgs e)
        {
            anagraficheLBox.Items.Clear();
            foreach (Anagrafica anagrafica in listaStazioni)
            {
                anagrafica.Score = 0;
                if (anagrafica.Nome.ToLower().Contains(cercaTBox.Text.ToLower())) anagrafica.Score += 10;
                if (anagrafica.NomeBreve.ToLower().Contains(cercaTBox.Text.ToLower())) anagrafica.Score += 5;
                foreach (char c in cercaTBox.Text.ToLower()) if (anagrafica.Nome.ToLower().Contains(c)) anagrafica.Score += 0.2;
                foreach (char c in cercaTBox.Text.ToLower()) if (anagrafica.NomeBreve.ToLower().Contains(c)) anagrafica.Score += 0.1;
            }
            listaStazioni.Sort((x, y) => y.Score.CompareTo(x.Score));
            foreach (Anagrafica anagrafica in listaStazioni) anagraficheLBox.Items.Add(anagrafica.Nome);
        }

        private void cercaTBox_TextChanged(object sender, EventArgs e)
        {
            if (cercaTBox.Text.Trim().Length == 0)
            {
                anagraficheLBox.Items.Clear();
                listaStazioni.Sort((y, x) => y.Nome.CompareTo(x.Nome));
                foreach (Anagrafica anagrafica in listaStazioni) anagraficheLBox.Items.Add(anagrafica.Nome);
            }
        }

        private void anagraficheLBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            Brush myBrush = Brushes.Black;
            var item = anagraficheLBox.Items[e.Index];
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                //e.Graphics.FillRectangle(new SolidBrush(Color.CadetBlue), e.Bounds);
            }
            else if (listaStazioni[e.Index].Score > 5 && cercaTBox.Text.Trim().Length > 0)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.LightYellow), e.Bounds);
            }
            else if (e.Index % 2 == 0)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(235, 235, 235)), e.Bounds);
            }
            e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(), e.Font, myBrush, e.Bounds, StringFormat.GenericDefault);
            e.DrawFocusRectangle();
        }

        private void cercaTBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                cercaBtn_Click(sender, new EventArgs());
            }
        }

        private PlotModel getModelTemperature(DatiStazione dati)
        {
            PlotModel model = new PlotModel();
            if (dati.Temperature.Count != 0)
            {
                LineSeries series = new LineSeries() { StrokeThickness = 1, MarkerSize = 1, Title = "Temperature [" + dati.Temperature[0].Um + "]", Color = OxyColor.FromUInt32((uint)Color.Purple.ToArgb()) };
                double min = dati.Temperature[0].T, max = dati.Temperature[0].T;
                for (int j = 0; j < dati.Temperature.Count; j++)
                {
                    Temperatura item = dati.Temperature[j];
                    series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(item.Data), item.T));
                    if (item.T > max) max = item.T; else if (item.T < min) min = item.T;
                }
                model.Series.Add(series);
                model.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Left,
                    Maximum = max + (max - min) / 5,
                    Minimum = min - (max - min) / 5,
                    IsZoomEnabled = false
                });
                model.Axes.Add(new DateTimeAxis
                {
                    Position = AxisPosition.Bottom,
                    AbsoluteMinimum = DateTimeAxis.ToDouble(dati.Temperature[0].Data),
                    AbsoluteMaximum = DateTimeAxis.ToDouble(dati.Temperature[dati.Temperature.Count - 1].Data),
                    StringFormat = "M/d H:m",
                    IsZoomEnabled = false
                });
            }
            return model;
        }
        private PlotModel getModelPrecipitazioni(DatiStazione dati)
        {
            if (dati.Precipitazioni.Count != 0)
            {
                PlotModel model = new PlotModel();
                LineSeries series = new LineSeries() { StrokeThickness = 1, MarkerSize = 1, Title = "Precipitazioni [" + dati.Precipitazioni[0].Um + "]", Color = OxyColor.FromUInt32((uint)Color.DarkBlue.ToArgb()) };
                double min = dati.Precipitazioni[0].P, max = dati.Precipitazioni[0].P;
                for (int j = 0; j < dati.Precipitazioni.Count; j++)
                {
                    Precipitazioni item = dati.Precipitazioni[j];
                    series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(item.Data), item.P));
                    if (item.P > max) max = item.P; else if (item.P < min) min = item.P;
                }
                model.Series.Add(series);
                model.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Left,
                    Maximum = max + (max - min) / 5,
                    Minimum = min - (max - min) / 5,
                    IsZoomEnabled = false
                });
                model.Axes.Add(new DateTimeAxis
                {
                    Position = AxisPosition.Bottom,
                    AbsoluteMinimum = DateTimeAxis.ToDouble(dati.Precipitazioni[0].Data),
                    AbsoluteMaximum = DateTimeAxis.ToDouble(dati.Precipitazioni[dati.Precipitazioni.Count - 1].Data),
                    StringFormat = "M/d H:m",
                    IsZoomEnabled = false
                });
                return model;
            }
            else
            {
                return null;
            }
        }
        private PlotModel getModelVenti(DatiStazione dati)
        {
            if (dati.Venti.Count != 0)
            {
                PlotModel model = new PlotModel();
                double min = Math.Min(dati.Venti[0].V, dati.Venti[0].D), max = Math.Max(dati.Venti[0].V, dati.Venti[0].D);
                LineSeries series = new LineSeries() { StrokeThickness = 1, MarkerSize = 1, Title = "Venti [" + dati.Venti[0].Um + "]", Color = OxyColor.FromUInt32((uint)Color.ForestGreen.ToArgb()) };
                for (int j = 0; j < dati.Venti.Count; j++)
                {
                    Venti item = dati.Venti[j];
                    series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(item.Data), item.V));
                    if (item.V > max) max = item.V; else if (item.V < min) min = item.V;
                }
                model.Series.Add(series);
                series = new LineSeries() { StrokeThickness = 1, MarkerSize = 1, Title = "Direzione [gN]", Color = OxyColor.FromRgb(50, 10, 10) };
                for (int j = 0; j < dati.Venti.Count; j++)
                {
                    Venti item = dati.Venti[j];
                    series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(item.Data), item.D));
                    if (item.D > max) max = item.D; else if (item.D < min) min = item.D;
                }
                model.Series.Add(series);
                model.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Left,
                    Maximum = max + (max - min) / 5,
                    Minimum = min - (max - min) / 5,
                    IsZoomEnabled = false
                });
                model.Axes.Add(new DateTimeAxis
                {
                    Position = AxisPosition.Bottom,
                    AbsoluteMinimum = DateTimeAxis.ToDouble(dati.Venti[0].Data),
                    AbsoluteMaximum = DateTimeAxis.ToDouble(dati.Venti[dati.Venti.Count - 1].Data),
                    StringFormat = "M/d H:m",
                    IsZoomEnabled = false
                });
                return model;
            }
            else
            {
                return null;
            }
        }
        private PlotModel getModelRadiazioni(DatiStazione dati)
        {
            if (dati.Radiazioni.Count != 0)
            {
                PlotModel model = new PlotModel();
                LineSeries series = new LineSeries() { StrokeThickness = 1, MarkerSize = 1, Title = "Radiazioni [" + dati.Radiazioni[0].Um + "]", Color = OxyColor.FromUInt32((uint)Color.Orange.ToArgb()) };
                double min = dati.Radiazioni[0].Rsg, max = dati.Radiazioni[0].Rsg;
                for (int j = 0; j < dati.Radiazioni.Count; j++)
                {
                    Radiazioni item = dati.Radiazioni[j];
                    series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(item.Data), item.Rsg));
                    if (item.Rsg > max) max = item.Rsg; else if (item.Rsg < min) min = item.Rsg;
                }
                model.Series.Add(series);
                model.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Left,
                    Maximum = max + (max - min) / 5,
                    Minimum = min - (max - min) / 5,
                    IsZoomEnabled = false
                });
                model.Axes.Add(new DateTimeAxis
                {
                    Position = AxisPosition.Bottom,
                    AbsoluteMinimum = DateTimeAxis.ToDouble(dati.Radiazioni[0].Data),
                    AbsoluteMaximum = DateTimeAxis.ToDouble(dati.Radiazioni[dati.Radiazioni.Count - 1].Data),
                    StringFormat = "M/d H:m",
                    IsZoomEnabled = false
                });
                return model;
            }
            else
            {
                return null;
            }
        }
        private PlotModel getModelUmidita(DatiStazione dati)
        {
            if (dati.UmiditaRelativa.Count != 0)
            {
                PlotModel model = new PlotModel();
                LineSeries series = new LineSeries() { StrokeThickness = 1, MarkerSize = 1, Title = "Umidità [" + dati.UmiditaRelativa[0].Um + "]", Color = OxyColor.FromUInt32((uint)Color.DarkCyan.ToArgb()) };
                double min = dati.UmiditaRelativa[0].Rh, max = dati.UmiditaRelativa[0].Rh;
                for (int j = 0; j < dati.UmiditaRelativa.Count; j++)
                {
                    UmiditaRelativa item = dati.UmiditaRelativa[j];
                    series.Points.Add(new DataPoint(DateTimeAxis.ToDouble(item.Data), item.Rh));
                    if (item.Rh > max) max = item.Rh; else if (item.Rh < min) min = item.Rh;
                }
                model.Series.Add(series);
                model.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Left,
                    Maximum = max + (max - min) / 5,
                    Minimum = min - (max - min) / 5,
                    IsZoomEnabled = false
                });
                model.Axes.Add(new DateTimeAxis
                {
                    Position = AxisPosition.Bottom,
                    AbsoluteMinimum = DateTimeAxis.ToDouble(dati.UmiditaRelativa[0].Data),
                    AbsoluteMaximum = DateTimeAxis.ToDouble(dati.UmiditaRelativa[dati.UmiditaRelativa.Count - 1].Data),
                    StringFormat = "M/d H:m",
                    IsZoomEnabled = false
                });
                return model;
            }
            else
            {
                return null;
            }
        }
        private void anagraficheLBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = anagraficheLBox.SelectedIndex;
            if (i != -1 && listaStazioni[i].Dati == null)
            {
                ScaricaDatiStazione(listaStazioni[i].Codice);
                listaStazioni[i].Dati = DatiStazione.GetDatiStazione(listaStazioni[i].Codice + ".xml");
            }

            nomeStazioneLbl.Text = listaStazioni[i].Nome;
            codiceStazioneLbl.Text = listaStazioni[i].NomeBreve + " - " + listaStazioni[i].Codice;
            latLongStazioneLbl.Text = listaStazioni[i].Quota + " mslm";
            latLongStazioneLbl.Text += "\nLat: \t" + listaStazioni[i].Latitudine;
            latLongStazioneLbl.Text += "\nLong: \t" + listaStazioni[i].Longitudine;
            varieStazioneLbl.Text = "Est: \t" + listaStazioni[i].East;
            varieStazioneLbl.Text += "\nNord: \t" + listaStazioni[i].North;
            varieStazioneLbl.Text += "\nInizio: \t" + listaStazioni[i].Inizio;
            varieStazioneLbl.Text += "\nFine: \t" + listaStazioni[i].Fine;
            if (listaStazioni[i].Dati != null)
            {
                nomeStazioneLbl.ForeColor = Color.Black;
                infoLbl.Text = "Data: " + listaStazioni[i].Dati.Data;
                infoLbl.Text += "     Max: " + listaStazioni[i].Dati.Tmax + " °C";
                infoLbl.Text += "     Min: " + listaStazioni[i].Dati.Tmin + " °C";
                infoLbl.Text += "     Pioggia: " + listaStazioni[i].Dati.Rain + " mm";

                tempPlot.Model = getModelTemperature(listaStazioni[i].Dati);
                precPlot.Model = getModelPrecipitazioni(listaStazioni[i].Dati);
                ventiPlot.Model = getModelVenti(listaStazioni[i].Dati);
                radPlot.Model = getModelRadiazioni(listaStazioni[i].Dati);
                umidPlot.Model = getModelUmidita(listaStazioni[i].Dati);
            }
            else
            {
                infoLbl.Text = "";
                nomeStazioneLbl.Text += " - La stazione non ha prodotto dati";
                nomeStazioneLbl.ForeColor = Color.Red;
                tempPlot.Model = null;
                precPlot.Model = null;
                ventiPlot.Model = null;
                radPlot.Model = null;
                umidPlot.Model = null;
            }
        }

        private DatiStazione ScaricaDatiStazione(string codiceStazione)
        {
            client.Proxy.Credentials = CredentialCache.DefaultCredentials;
            try
            {
                DatiStazione datiStazione = new DatiStazione();
                byte[] dati = client.DownloadData("http://dati.meteotrentino.it/service.asmx/ultimiDatiStazione?codice=" + codiceStazione);
                string datiStr = Encoding.ASCII.GetString(dati, 0, dati.Length);
                datiStazione.MtxAccessoAlFileXML.WaitOne();
                File.WriteAllText(codiceStazione + ".xml", datiStr);
                datiStazione.MtxAccessoAlFileXML.ReleaseMutex();
                return datiStazione;
            }
            catch
            {
                MessageBox.Show("Impossibile scaricare i dati, verificare il collegamento ad Internet.", "ERRORE");
                return null;
            }
        }
    }
}
